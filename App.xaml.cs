﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using TestWork.Common;
using TestWork.Infrastructure;
using TestWork.Models;
using TestWork.ViewModels.Windows;

namespace TestWork
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        private const string _paramNameOfConnectionString = "name=" + Constants.NAME_OF_CONNECTION_STRING;

        private enum DatabaseOpenResult
        {
            InProgress = 1,
            DatabaseExist,
            ExitRequested
        }

        private MainModel _mainModel;

        protected override async void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            try
            {
                _mainModel = new MainModel();

                DatabaseOpenResult databaseOpenResult = DatabaseOpenResult.InProgress;

                while (databaseOpenResult == DatabaseOpenResult.InProgress)
                {
                    MainModel.DatabaseStatus databaseStatus = await _mainModel.OpenDatabase(_paramNameOfConnectionString);

                    switch (databaseStatus)
                    {
                        case MainModel.DatabaseStatus.Exist:
                            databaseOpenResult = DatabaseOpenResult.DatabaseExist;
                            break;
                        case MainModel.DatabaseStatus.NotExist:
                            ShowMockWizard(_mainModel);
                            break;
                        case MainModel.DatabaseStatus.NotSet:
                        case MainModel.DatabaseStatus.NotFound:
                            databaseOpenResult = DatabaseOpenResult.ExitRequested;
                            throw new Exception($"Не найдена строка подключения {_paramNameOfConnectionString}");
                    }
                }

                switch (databaseOpenResult)
                {
                    case DatabaseOpenResult.DatabaseExist:
                        ShowMainWindow(_mainModel);
                        break;
                    case DatabaseOpenResult.ExitRequested:
                        Close();
                        break;
                }
            }
            catch (Exception exception)
            {
                ShowErrorWindow(exception);
                Close();
            }
        }

        private void SetupExceptionHandlers()
        {
            AppDomain.CurrentDomain.UnhandledException += (sender, e) => ShowErrorWindow(e.ExceptionObject as Exception);
            DispatcherUnhandledException += (sender, e) =>
            {
                ShowErrorWindow(e.Exception);
                e.Handled = true;
            };
            TaskScheduler.UnobservedTaskException += (sender, e) =>
            {
                ShowErrorWindow(e.Exception);
                e.SetObserved();
            };
        }

        private void ShowErrorWindow(Exception exception)
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => ShowErrorWindow(exception));
            }
            else
            {
                try
                {
                    ErrorWindowViewModel errorWindowViewModel = new ErrorWindowViewModel(exception?.ToString() ?? "(null)");
                    IWindowContext errorWindowContext = WindowManager.CreateWindow(RegisteredWindows.WindowKey.ErrorWindow, errorWindowViewModel);
                    errorWindowContext.ShowDialog();
                }
                catch (Exception errorWindowException)
                {
                    Console.WriteLine(errorWindowException);
                    throw;
                }
            }
        }

        private void ShowMockWizard(MainModel model)
        {
            var mockWizardViewModel = new MockWizardViewModel(_mainModel);

            IWindowContext windowContext = WindowManager.CreateWindow(RegisteredWindows.WindowKey.WizardWindow, mockWizardViewModel);

            windowContext.ShowDialog();
        }

        private void ShowMainWindow(MainModel model)
        {
            var mainViewModel = new MainWindowViewModel(model);

            IWindowContext windowContext = WindowManager.CreateWindow(RegisteredWindows.WindowKey.MainWindow,
                mainViewModel);
            windowContext.Closed += (sender, args) => Close();

            windowContext.Show();
        }

        private void Close()
        {
            Shutdown();
        }
    }
}
