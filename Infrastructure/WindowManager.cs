﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace TestWork.Infrastructure
{
    public class WindowManager
    {
        private static readonly List<WindowContext> _createdWindowContexts;

        static WindowManager()
        {
            _createdWindowContexts = new List<WindowContext>();
        }

        public static IWindowContext CreateWindow(RegisteredWindows.WindowKey windowKey, object viewModel = null, IWindowContext parentWindowContext = null)
        {
            RegisteredWindows.RegisteredWindow registeredWindow = RegisteredWindows.AllWindows[windowKey];
            if (viewModel == null)
            {
                viewModel = Activator.CreateInstance(registeredWindow.ViewModelType);
                if (viewModel == null)
                {
                    throw new InvalidOperationException($"Произошла ошибка при попытке создать экземпляр ViewModel {registeredWindow.WindowType.FullName}.");
                }
            }
            Window window = Activator.CreateInstance(registeredWindow.WindowType) as Window;
            if (window == null)
            {
                throw new InvalidOperationException($"Произошла ошибка при попытке создать экземпляр View {registeredWindow.WindowType.FullName}.");
            }
            window.DataContext = viewModel;
            Window parentWindow = (parentWindowContext as WindowContext)?.Window;
            WindowContext result = new WindowContext(window, viewModel, parentWindow);
            result.Closed += WindowContext_Closed;
            _createdWindowContexts.Add(result);
            return result;
        }

        public static IWindowContext GetWindowContext(object viewModel)
        {
            return _createdWindowContexts.FirstOrDefault(windowContext => ReferenceEquals(windowContext.DataContext, viewModel));
        }

        private static void WindowContext_Closed(object sender, EventArgs e)
        {
            WindowContext closedWindowContext = (WindowContext)sender;
            _createdWindowContexts.Remove(closedWindowContext);
            closedWindowContext.Closed -= WindowContext_Closed;
        }
    }
}