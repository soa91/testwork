﻿namespace TestWork.Infrastructure
{
    public enum MainWindowState
    {
        None,
        Cities,
        Streets,
        Houses,
        Apartments
    }
}