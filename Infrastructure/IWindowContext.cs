﻿using System;

namespace TestWork.Infrastructure
{
    public interface IWindowContext
    {
        object DataContext { get; }

        event EventHandler Closed;

        void Close();
        void CloseDialog(bool dialogResult);
        void Show(int? width = null, int? height = null, bool showMaximized = false);
        bool? ShowDialog(int? width = null, int? height = null, bool showMaximized = false);
    }
}
