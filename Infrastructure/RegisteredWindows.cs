﻿using System;
using System.Collections.Generic;
using TestWork.ViewModels.Windows;
using TestWork.Views.Windows;

namespace TestWork.Infrastructure
{
    public class RegisteredWindows
    {
        public enum WindowKey
        {
            MainWindow = 1,
            WizardWindow,
            ErrorWindow
        }

        public class RegisteredWindow
        {
            public WindowKey WindowKey { get; }
            public Type WindowType { get; }
            public Type ViewModelType { get; }

            public RegisteredWindow(WindowKey windowKey, Type windowType, Type viewModelType)
            {
                WindowKey = windowKey;
                WindowType = windowType;
                ViewModelType = viewModelType;
            }
        }

        public static Dictionary<WindowKey, RegisteredWindow> AllWindows { get; }

        static RegisteredWindows()
        {
            AllWindows = new Dictionary<WindowKey, RegisteredWindow>();
            RegisterWindow(WindowKey.MainWindow, typeof(MainWindowView), typeof(MainWindowViewModel));
            RegisterWindow(WindowKey.ErrorWindow, typeof(ErrorWindowView), typeof(ErrorWindowViewModel));
            RegisterWindow(WindowKey.WizardWindow, typeof(MockWizardView), typeof(MockWizardViewModel));
        }

        private static void RegisterWindow(WindowKey windowKey, Type windowType, Type viewModelType)
        {
            AllWindows.Add(windowKey, new RegisteredWindow(windowKey, windowType, viewModelType));
        }
    }
}