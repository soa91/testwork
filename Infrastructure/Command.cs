﻿using System;
using System.Windows.Input;

namespace TestWork.Infrastructure
{
    public class Command : ICommand
    {
        private readonly Predicate<object> _canExecute = null;
        private readonly Action<Object> _executeAction = null;

        public Command(Action executeAction)
            : this(param => true, param => executeAction())
        {
        }

        public Command(Action<object> executeAction)
            : this(param => true, executeAction)
        {
        }

        public Command(Predicate<object> canExecute, Action<object> executeAction)
        {
            _canExecute = canExecute;
            _executeAction = executeAction;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            if (_canExecute != null)
            {
                return _canExecute(parameter);
            }
            return true;
        }

        public void Execute(object parameter)
        {
            _executeAction?.Invoke(parameter);
            UpdateCanExecuteState();
        }

        public void UpdateCanExecuteState()
        {
            CanExecuteChanged?.Invoke(this, new EventArgs());
        }
    }
}
