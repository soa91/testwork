﻿using System;
using System.Windows;

namespace TestWork.Infrastructure
{
    public class WindowContext : IWindowContext
    {
        private readonly Window _parentWindow;
        public Window Window { get; }
        public object DataContext { get; }
        public event EventHandler Closed;

        public WindowContext(Window window, object dataContext, Window parentWindow)
        {
            Window = window;
            DataContext = dataContext;
            _parentWindow = parentWindow;
            Window.Closed += Window_Closed;
        }

        public void Show(int? width = null, int? height = null, bool showMaximized = false)
        {
            if (!Window.IsVisible)
            {
                if (width.HasValue)
                {
                    Window.Width = width.Value;
                }
                if (height.HasValue)
                {
                    Window.Height = height.Value;
                }

                Window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                Window.Show();
            }
            else
            {
                if (Window.WindowState == WindowState.Minimized)
                {
                    Window.WindowState = WindowState.Normal;
                }
                else
                {
                    Window.Activate();
                }
            }
        }

        public bool? ShowDialog(int? width = null, int? height = null, bool showMaximized = false)
        {
            //OnShowing();
            if (width.HasValue)
            {
                Window.Width = width.Value;
            }
            if (height.HasValue)
            {
                Window.Height = height.Value;
            }
            if (_parentWindow != null)
            {
                Window.Owner = _parentWindow;
                Window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            }
            else
            {
                Window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            }
            return ShowDialog(showMaximized, _parentWindow == null);
        }

        private bool? ShowDialog(bool showMaximized, bool showInTaskbar)
        {
            Window.ShowInTaskbar = showInTaskbar;
            return Window.ShowDialog();
        }

        public void Close()
        {
            Window.Close();
        }

        public void CloseDialog(bool dialogResult)
        {
            Window.DialogResult = dialogResult;
        }

        protected virtual void OnClosed()
        {
            Closed?.Invoke(this, EventArgs.Empty);
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            OnClosed();
            Window.Closed -= Window_Closed;
        }
    }
}