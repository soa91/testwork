﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestWork.Models.Database;
using TestWork.Models.Entities;

namespace TestWork.Models
{
    public class MainModel
    {
        public enum DatabaseStatus
        {
            Exist = 1,
            NotExist,
            NotSet,
            NotFound
        }

        private LocalDatabase _localDatabase;

        public MainModel()
        {

        }

        public Task CreateDatabase()
        {
            return Task.Run(() =>
            {
                _localDatabase.Initializer();
            });
        }

        public Task<DatabaseStatus> OpenDatabase(string nameOfConnectionString)
        {
            return Task.Run(() =>
            {
                if (_localDatabase != null)
                {
                    _localDatabase = null;
                }

                if (!string.IsNullOrWhiteSpace(nameOfConnectionString))
                {
                    _localDatabase = LocalDatabase.OpenDatabase(nameOfConnectionString);

                    try
                    {
                        if (!_localDatabase.CheckExistConnectionString())
                        {
                            return DatabaseStatus.NotFound;
                        }

                        if (_localDatabase.CheckExist())
                        {
                            return DatabaseStatus.Exist;
                        }
                        else
                        {
                            return DatabaseStatus.NotExist;
                        }
                    }
                    catch (Exception e)
                    {
                        return DatabaseStatus.NotFound;
                    }
                }
                else
                {
                    return DatabaseStatus.NotSet;
                }
            });
        }

        public Task<List<City>> RequestGetAllCitiesAsync()
        {
            return RequestAsync(_localDatabase.GetAllCities);
        }

        public Task<List<Street>> SearchAllStreetsAsync(City city)
        {
            return SearchItemsAsync(_localDatabase.GetManyStreets, city);
        }

        public Task<List<House>> SearchAllHousesAsync(Street street)
        {
            return SearchItemsAsync(_localDatabase.GetManyHouses, street);
        }

        public Task<List<Apartment>> SearchAllHousesAsync(House house)
        {
            return SearchItemsAsync(_localDatabase.GetManyApartments, house);
        }

        public Task<List<Apartment>> GetSliceApartmentAsync(House house, decimal min, decimal max)
        {
            return GetSliceItemsAsync(_localDatabase.GetSliceApartments, house, min, max);
        }

        //TODO: переписать на более универсаьный вариант с передачей запроса, вида Expression<Func<T, bool>> @where

        private Task<List<T>> RequestAsync<T>(Func<IEnumerable<T>> function)
        {
            return Task.Run(() =>
            {
                return function().ToList();
            });
        }

        private Task<List<T1>> SearchItemsAsync<T1, T2>(Func<T2, IEnumerable<T1>> functions, T2 entityObject)
        {
            return Task.Run(() =>
            {
                var result = new List<T1>();

                foreach (var t1 in functions(entityObject))
                {
                    result.Add(t1);
                }

                return result;
            });
        }

        private Task<List<Apartment>> GetSliceItemsAsync(Func<House, decimal, decimal, IEnumerable<Apartment>> function, House house, decimal min, decimal max)
        {
            return Task.Run(() =>
            {
                return function(house, min, max).ToList();
            });
        }
    }
}
