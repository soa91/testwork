﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TestWork.Models.Entities
{
    public class House
    {
        [Column("id")]
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        [Column("number")]
        public string Number { get; set; }
        [Column("street_id")]
        public int StreetId { get; set; }
        public virtual Street Street { get; set; }
        public virtual ICollection<Apartment> Apartments{ get; set; }
    }
}