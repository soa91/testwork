﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TestWork.Models.Entities
{
    public class City
    {
        public City()
        {
            Streets = new HashSet<Street>();
        }

        [Column("id")]
        public int Id { get; set; }
        [Required]
        [MaxLength(100)]
        [Column("name")]
        public string Name { get; set; }
        public virtual ICollection<Street> Streets { get; set; }
    }
}