﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TestWork.Models.Entities
{
    public class Apartment
    {
        [Column("id")]
        public int Id { get; set; }
        
        [Required] //Precision(10,2) in OnModelCreating
        [Column("area")]
        public decimal Area { get; set; }
        [Column("house_id")]
        public int HouseId { get; set; }
        public virtual House House{ get; set; }
    }
}