﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TestWork.Models.Entities
{
    public class Street
    {
        public Street()
        {
            Houses = new HashSet<House>();
        }

        [Column("id")]
        public int Id { get; set; }
        [Required]
        [MaxLength(100)]
        [Column("name")]
        public string Name { get; set; }
        [Column("city_id")]
        public int CityId { get; set; }
        public virtual City City { get; set; }
        public virtual ICollection<House> Houses { get; set; }
    }
}