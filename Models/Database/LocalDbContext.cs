﻿using System.Data.Entity;
using System.Windows.Documents;
using TestWork.Models.Entities;

namespace TestWork.Models.Database
{
    public class LocalDbContext : DbContext
    {
        public LocalDbContext(string nameOfConnectionString) : base(nameOfConnectionString)
        {
            System.Data.Entity.Database.SetInitializer(new LocalDbInitializer());
        }

        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<House> Houses { get; set; }
        public virtual DbSet<Street> Streets { get; set; }
        public virtual DbSet<Apartment> Apartments { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Apartment>()
                .Property(e => e.Area)
                .HasPrecision(10, 2);

            modelBuilder.Entity<House>()
                .HasMany(e => e.Apartments)
                .WithRequired(e => e.House)
                .HasForeignKey(e => e.HouseId);

            modelBuilder.Entity<Street>()
                .HasMany(e => e.Houses)
                .WithRequired(e => e.Street)
                .HasForeignKey(e => e.StreetId);
            
            modelBuilder.Entity<City>()
                .HasMany(e => e.Streets)
                .WithRequired(e => e.City)
                .HasForeignKey(e => e.CityId);
        }
    }
}