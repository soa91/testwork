﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Text;
using TestWork.Models.Entities;

namespace TestWork.Models.Database
{
    public class LocalDbInitializer : DropCreateDatabaseIfModelChanges<LocalDbContext>
    {
        private Random _random;
        protected override void Seed(LocalDbContext context)
        {
            _random = new Random();

            #region cities
            var city1 = new City() {Name = "Ярославль"};
            var city2 = new City() {Name = "Рыбинск"};
            var city3 = new City() {Name = "Данилов"};

            var cities = new List<City>()
            {
                city1, city2, city3
            };

            context.Cities.AddRange(cities);
            #endregion

            #region streets
            var street11 = new Street() {City = city1, Name = "Центральная"};
            var street12 = new Street() {City = city1, Name = "Советская"};
            var street13 = new Street() {City = city1, Name = "Школьная"};
            var street14 = new Street() {City = city1, Name = "Садовая"};
            var street15 = new Street() {City = city1, Name = "Мира"};

            var street21 = new Street() {City = city2, Name = "Молодежная"};
            var street22 = new Street() {City = city2, Name = "Лесная"};
            var street23 = new Street() {City = city2, Name = "Заречная"};
            var street24 = new Street() {City = city2, Name = "Советская"};
            var street25 = new Street() {City = city2, Name = "Ленина"};

            var street31 = new Street() {City = city3, Name = "Центральная"};
            var street32 = new Street() {City = city3, Name = "Новая"};
            var street33 = new Street() {City = city3, Name = "Зеленая"};
            var street34 = new Street() {City = city3, Name = "Октябрьская"};
            var street35 = new Street() {City = city3, Name = "Молодежная"};

            var streets = new List<Street>()
            {
                street11, street12, street13, street14, street15,
                street21, street22, street23, street24, street25,
                street31, street32, street33, street34, street35
            };

            context.Streets.AddRange(streets);
            #endregion

            #region houses
            var houses = new List<House>();
            
            foreach (var street in streets)
            {
                foreach (var houseNumber in GenerateListHouseNumbers(GetRandomNumber(2, 5)))
                {
                    houses.Add(new House { Street = street, Number = houseNumber });
                }
            }

            context.Houses.AddRange(houses);
            #endregion

            #region apartments
            var apartments = new List<Apartment>();

            foreach (var house in houses)
            {
                for (int i = 0; i < GetRandomNumber(2, 5); i++)
                {
                    apartments.Add(new Apartment(){House = house, Area = GetRandomArea()});
                }
            }

            context.Apartments.AddRange(apartments);
            #endregion

            _random = null;

            context.SaveChanges();

            base.Seed(context);
        }

        private List<string> GenerateListHouseNumbers(int count)
        {
            var result = new List<string>();

            while (count > 0)
            {
                var sb = new StringBuilder();

                var number = GetRandomNumber(1, 99);

                var hasChar = GetRandomTrueFalse();

                sb.Append(number);
                if (hasChar)
                {
                    sb.Append(GetRandomLetter());
                }
                else
                {
                    var hasFract = GetRandomTrueFalse();
                    if (hasFract)
                    {
                        sb.Append("/");
                        sb.Append(GetRandomNumber(1, 99));
                    }
                }

                var resultStr = sb.ToString();

                if (result.Contains(resultStr)) continue;

                result.Add(resultStr);
                count--;
            }


            return result;
        }

        private int GetRandomNumber(int min, int max)
        {
            var number = _random.Next(min, max);

            return number;
        }

        private decimal GetRandomArea()
        {
            return new decimal(_random.Next(15, 70) + _random.NextDouble());
        }

        private char GetRandomLetter()
        {
            string chars = "АБВГДЕЖЗИКЛМНОПРСТУЦЭЮЯ";
            int num = GetRandomNumber(0, chars.Length);

            return chars[num];
        }

        private bool GetRandomTrueFalse()
        {
            return GetRandomNumber(0, 100) <= 50;
        }
    }
}