﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TestWork.Models.Entities;

namespace TestWork.Models.Database
{
    public class LocalDatabase
    {
        private readonly string _nameOfConnectionString;

        protected LocalDatabase(string nameOfConnectionString)
        {
            _nameOfConnectionString = nameOfConnectionString;
        }

        public static LocalDatabase OpenDatabase(string nameOfConnectionString)
        {
            return new LocalDatabase(nameOfConnectionString);
        }

        public bool CheckExistConnectionString()
        {
            using (var context = new LocalDbContext(_nameOfConnectionString))
            {
                return !string.IsNullOrWhiteSpace(context?.Database?.Connection?.ConnectionString);
            }
        }

        public bool CheckExist()
        {
            using (var context = new LocalDbContext(_nameOfConnectionString))
            {
                return context.Database.Exists();
            }
        }

        public void Initializer()
        {
            using (var context = new LocalDbContext(_nameOfConnectionString))
            {
                context.Database.Initialize(true);
            }
        }

        public IEnumerable<City> GetAllCities()
        {
            using (var context = new LocalDbContext(_nameOfConnectionString))
            {
                return context.Cities.Include(c => c.Streets).ToList();
            }
        }

        public IEnumerable<Street> GetManyStreets(City city)
        {
            using (var context = new LocalDbContext(_nameOfConnectionString))
            {
                return context.Streets.Include(s => s.Houses).Where(s => s.CityId == city.Id).ToList();
            }
        }

        public IEnumerable<House> GetManyHouses(Street street)
        {
            using (var context = new LocalDbContext(_nameOfConnectionString))
            {
                return context.Houses.Include(h => h.Apartments).Where(h => h.StreetId == street.Id).ToList();
            }
        }

        public IEnumerable<Apartment> GetManyApartments(House house)
        {
            using (var context = new LocalDbContext(_nameOfConnectionString))
            {
                return context.Apartments.Where(a => a.HouseId == house.Id).ToList();
            }
        }

        public IEnumerable<Apartment> GetSliceApartments(House house, decimal min, decimal max)
        {
            using (var context = new LocalDbContext(_nameOfConnectionString))
            {
                return context.Apartments.Where(a => a.HouseId == house.Id && a.Area >= min && a.Area <= max).ToList();
            }
        }
    }
}