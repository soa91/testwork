﻿using System.Linq;
using TestWork.Models.Entities;

namespace TestWork.ViewModels.RequestResultItems
{
    public class HouseRequestResultItemViewModel : RequestResultItemViewModel<House>
    {
        public HouseRequestResultItemViewModel(House appObject) : base(appObject)
        {
        }

        public int Id
        {
            get { return AppObject.Id; }
        }
        public string Number
        {
            get { return AppObject.Number; }
        }
        public int ApartmentCount
        {
            get { return AppObject.Apartments.Count; }
        }
        public decimal HouseArea
        {
            get
            {
                return AppObject.Apartments.Sum(appObjectApartment => appObjectApartment.Area);
            }
        }
    }
}