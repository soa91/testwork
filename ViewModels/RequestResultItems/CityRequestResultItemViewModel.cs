﻿using TestWork.Models.Entities;

namespace TestWork.ViewModels.RequestResultItems
{
    public class CityRequestResultItemViewModel : RequestResultItemViewModel<City>
    {
        public CityRequestResultItemViewModel(City appObject) : base(appObject)
        {
        }

        public int Id
        {
            get { return AppObject.Id; }
        }
        public string Name
        {
            get { return AppObject.Name; }
        }
        public int StreetCount
        {
            get { return AppObject.Streets.Count; }
        }
    }
}