﻿namespace TestWork.ViewModels.RequestResultItems
{
    public abstract class RequestResultItemViewModel<T> : ViewModel
    {
        public RequestResultItemViewModel(T appObject)
        {
            AppObject = appObject;
        }

        public T AppObject { get; }
    }
}
