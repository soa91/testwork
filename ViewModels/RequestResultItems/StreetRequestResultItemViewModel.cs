﻿using TestWork.Models.Entities;

namespace TestWork.ViewModels.RequestResultItems
{
    public class StreetRequestResultItemViewModel : RequestResultItemViewModel<Street>
    {
        public StreetRequestResultItemViewModel(Street appObject) : base(appObject)
        {
        }

        public int Id
        {
            get { return AppObject.Id; }
        }
        public string Name
        {
            get { return AppObject.Name; }
        }
        public int HouseCount
        {
            get { return AppObject.Houses.Count; }
        }
    }
}