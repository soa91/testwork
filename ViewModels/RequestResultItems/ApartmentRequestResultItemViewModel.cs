﻿using TestWork.Models.Entities;

namespace TestWork.ViewModels.RequestResultItems
{
    public class ApartmentRequestResultItemViewModel : RequestResultItemViewModel<Apartment>
    {
        public ApartmentRequestResultItemViewModel(Apartment appObject) : base(appObject)
        {
        }

        public int Id
        {
            get { return AppObject.Id; }
        }
        public decimal Area
        {
            get { return AppObject.Area; }
        }
    }
}