﻿using System;
using TestWork.Infrastructure;
using TestWork.Models;
using TestWork.ViewModels.Windows;

namespace TestWork.ViewModels
{
    public abstract class ContainerViewModel : ViewModel
    {
        protected ContainerViewModel(MainModel mainModel)
        {
            MainModel = mainModel;
        }

        protected MainModel MainModel { get; }

        protected void ShowErrorWindow(Exception exception, IWindowContext parentWindowContext)
        {
            ErrorWindowViewModel errorWindowViewModel = new ErrorWindowViewModel(exception.ToString());

            IWindowContext errorWindowContext = WindowManager.CreateWindow(RegisteredWindows.WindowKey.ErrorWindow, errorWindowViewModel, parentWindowContext);

            errorWindowContext.ShowDialog();
        }
    }
}