﻿using TestWork.Infrastructure;
using TestWork.Models;

namespace TestWork.ViewModels.Windows
{
    public class MockWizardViewModel : AppWindowViewModel
    {
        public MockWizardViewModel(MainModel mainModel) : base(mainModel)
        {
            EmptyCommand = new Command(() => { });

            CreateDatabaseAndCloseDialog();
        }
        
        public Command EmptyCommand { get; }

        private async void CreateDatabaseAndCloseDialog()
        {
            await MainModel.CreateDatabase();
 
            CurrentWindowContext.CloseDialog(true);
        }
    }
}