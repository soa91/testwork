﻿using TestWork.Infrastructure;
using TestWork.Models;

namespace TestWork.ViewModels.Windows
{
    public abstract class AppWindowViewModel : ContainerViewModel
    {
        protected AppWindowViewModel(MainModel mainModel) : base(mainModel)
        {

        }

        protected IWindowContext CurrentWindowContext
        {
            get
            {
                return WindowManager.GetWindowContext(this);
            }
        }
    }
}
