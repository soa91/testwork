﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using TestWork.Infrastructure;
using TestWork.Models;
using TestWork.Models.Entities;
using TestWork.ViewModels.RequestResultItems;
using TestWork.ViewModels.States;
using TestWork.Views.Controls;

namespace TestWork.ViewModels.Windows
{
    public class MainWindowViewModel : AppWindowViewModel
    {
        private IMainWindowViewModelState _selectedState;

        private ObservableCollection<CityRequestResultItemViewModel> _cities;
        private ObservableCollection<StreetRequestResultItemViewModel> _streets;
        private ObservableCollection<HouseRequestResultItemViewModel> _houses;
        private ObservableCollection<ApartmentRequestResultItemViewModel> _apartments;

        private CityRequestResultItemViewModel _selectedCity;
        private StreetRequestResultItemViewModel _selectedStreet;
        private HouseRequestResultItemViewModel _selectedHouse;
        private ApartmentRequestResultItemViewModel _selectedApartment;

        public MainWindowViewModel(MainModel mainModel) : base(mainModel)
        {
            StateCities = new MainWindowViewModelStateCities(this);
            StateStreets = new MainWindowViewModelStateStreets(this);
            StateHouses = new MainWindowStateHouses(this);
            StateApartments = new MainWindowViewModelStateApartments(this);

            _selectedState = StateCities;

            Cities = new ObservableCollection<CityRequestResultItemViewModel>();
            Streets = new ObservableCollection<StreetRequestResultItemViewModel>();
            Houses = new ObservableCollection<HouseRequestResultItemViewModel>();
            Apartments = new ObservableCollection<ApartmentRequestResultItemViewModel>();

            BackCommand = new Command(Back);
            SelectCityCommand = new Command(SelectCity);
            SelectStreetCommand = new Command(SelectStreet);
            SelectHouseCommand = new Command(SelectHouse);
            SliceApartmentCommand = new Command(SliceApartment);
        }
        
        public IMainWindowViewModelState StateCities { get; }
        public IMainWindowViewModelState StateStreets { get; }
        public IMainWindowViewModelState StateHouses { get; }
        public IMainWindowViewModelState StateApartments { get; }

        public MainWindowState CurrentState
        {
            get { return _selectedState.CurrentMainWindowState; }
        }
        public bool VisibleBack
        {
            get { return _selectedState.VisibleBack; }
        }
        public bool VisibleFilter
        {
            get { return _selectedState.VisibleFilter; }
        }

        public decimal ApartmentAreaMin { get; set; }
        public decimal ApartmentAreaMax { get; set; }

        public ObservableCollection<CityRequestResultItemViewModel> Cities
        {
            get { return _cities; }
            set
            {
                if (_cities != value)
                {
                    _cities = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public ObservableCollection<StreetRequestResultItemViewModel> Streets
        {
            get { return _streets; }
            set
            {
                if (_streets != value)
                {
                    _streets = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public ObservableCollection<HouseRequestResultItemViewModel> Houses
        {
            get { return _houses; }
            set
            {
                if (_houses != value)
                {
                    _houses = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public ObservableCollection<ApartmentRequestResultItemViewModel> Apartments
        {
            get { return _apartments; }
            set
            {
                if (_apartments != value)
                {
                    _apartments = value;
                    NotifyPropertyChanged();
                }
            }
        }
        
        public CityRequestResultItemViewModel SelectedCity
        {
            get { return _selectedCity; }
            set
            {
                if (_selectedCity != value)
                {
                    _selectedCity = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public StreetRequestResultItemViewModel SelectedStreet
        {
            get { return _selectedStreet; }
            set
            {
                if (_selectedStreet != value)
                {
                    _selectedStreet = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public ApartmentRequestResultItemViewModel SelectedApartment
        {
            get { return _selectedApartment; }
            set
            {
                if (_selectedApartment != value)
                {
                    _selectedApartment = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public HouseRequestResultItemViewModel SelectedHouse
        {
            get { return _selectedHouse; }
            set
            {
                if (_selectedHouse != value)
                {
                    _selectedHouse = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public Command BackCommand { get; }
        public Command SelectCityCommand { get; }
        public Command SelectStreetCommand { get; }
        public Command SelectHouseCommand { get; }
        public Command SliceApartmentCommand { get; }

        public void SetState(IMainWindowViewModelState state)
        {
            _selectedState = state;

            NotifyPropertyChanged(nameof(CurrentState));
            NotifyPropertyChanged(nameof(VisibleBack));
            NotifyPropertyChanged(nameof(VisibleFilter));
        }

        private void Back()
        {
            _selectedState.Back();
        }

        private void SelectCity()
        {
            _selectedState.SelectCity();
        }

        private void SelectStreet()
        {
            _selectedState.SelectStreet();
        }

        private void SelectHouse()
        {
            _selectedState.SelectHouse();
        }

        private void SliceApartment()
        {
            _selectedState.SliceApartment();
        }

        public async Task GetAllCitiesASync()
        {
            var result = new List<City>();

            try
            {
                result = await MainModel.RequestGetAllCitiesAsync();
            }
            catch (Exception e)
            {
                ShowErrorWindow(e, CurrentWindowContext);
            }

            Cities = new ObservableCollection<CityRequestResultItemViewModel>(result.Select(c => new CityRequestResultItemViewModel(c)));
        }

        public async Task SearchAllStreetsAsync(City city)
        {
            var result = new List<Street>();

            try
            {
                result = await MainModel.SearchAllStreetsAsync(city);
            }
            catch (Exception e)
            {
                ShowErrorWindow(e, CurrentWindowContext);
            }

            Streets = new ObservableCollection<StreetRequestResultItemViewModel>(result.Select(s => new StreetRequestResultItemViewModel(s)));
        }

        public async Task SearchAllHousesAsync(Street street)
        {
            var result = new List<House>();

            try
            {
                result = await MainModel.SearchAllHousesAsync(street);
            }
            catch (Exception e)
            {
                ShowErrorWindow(e, CurrentWindowContext);
            }

            Houses = new ObservableCollection<HouseRequestResultItemViewModel>(result.Select(h => new HouseRequestResultItemViewModel(h)));
        }

        public async Task SearchAllApartmentsAsync(House house)
        {
            var result = new List<Apartment>();

            try
            {
                result = await MainModel.SearchAllHousesAsync(house);
            }
            catch (Exception e)
            {
                ShowErrorWindow(e, CurrentWindowContext);
            }

            Apartments = new ObservableCollection<ApartmentRequestResultItemViewModel>(result.Select(a => new ApartmentRequestResultItemViewModel(a)));
        }

        public async Task SliceApartmentsAsync(House house, decimal min, decimal max)
        {
            var result = new List<Apartment>();

            try
            {
                result = await MainModel.GetSliceApartmentAsync(house, min, max);
            }
            catch (Exception e)
            {
                ShowErrorWindow(e, CurrentWindowContext);
            }

            Apartments = new ObservableCollection<ApartmentRequestResultItemViewModel>(result.Select(a => new ApartmentRequestResultItemViewModel(a)));
        }
    }
}
