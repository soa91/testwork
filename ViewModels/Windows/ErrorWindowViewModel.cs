﻿using TestWork.ViewModels;

namespace TestWork.ViewModels.Windows
{
    public class ErrorWindowViewModel : ViewModel
    {
        public ErrorWindowViewModel(string error)
        {
            Error = error;
        }

        public string Error { get; }
    }
}