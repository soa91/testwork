﻿using TestWork.Infrastructure;
using TestWork.ViewModels.Windows;

namespace TestWork.ViewModels.States
{
    public class MainWindowViewModelStateCities : IMainWindowViewModelState
    {
        private MainWindowViewModel _mainViewModel;

        public MainWindowViewModelStateCities(MainWindowViewModel mainWindowViewModel)
        {
            _mainViewModel = mainWindowViewModel;
            CurrentMainWindowState = MainWindowState.Cities;
            VisibleBack = false;
            VisibleFilter = false;
            
            LoadCities();
        }

        public MainWindowState CurrentMainWindowState { get; }
        public bool VisibleBack { get; }
        public bool VisibleFilter { get; }

        public void Back()
        {

        }

        public void SelectCity()
        {
            if (_mainViewModel.SelectedCity == null) return;

            LoadStreets();

            _mainViewModel.SetState(_mainViewModel.StateStreets);
        }

        public void SelectStreet()
        {

        }

        public void SelectHouse()
        {

        }

        public void SliceApartment()
        {

        }
        
        private async void LoadStreets()
        {
            await _mainViewModel.SearchAllStreetsAsync(_mainViewModel.SelectedCity.AppObject);
        }

        private async void LoadCities()
        {
            await _mainViewModel.GetAllCitiesASync();
        }
    }
}