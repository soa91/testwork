﻿using TestWork.Infrastructure;
using TestWork.ViewModels.Windows;

namespace TestWork.ViewModels.States
{
    public class MainWindowViewModelStateStreets : IMainWindowViewModelState
    {
        private MainWindowViewModel _mainViewModel;

        public MainWindowViewModelStateStreets(MainWindowViewModel mainWindowViewModel)
        {
            _mainViewModel = mainWindowViewModel;
            CurrentMainWindowState = MainWindowState.Streets;
            VisibleBack = true;
            VisibleFilter = false;
        }

        public MainWindowState CurrentMainWindowState { get; }
        public bool VisibleBack { get; }
        public bool VisibleFilter { get; }

        public void Back()
        {
            _mainViewModel.SetState(_mainViewModel.StateCities);
        }

        public void SelectCity()
        {

        }

        public void SelectStreet()
        {
            if (_mainViewModel.SelectedStreet == null) return;

            LoadHouses();

            _mainViewModel.SetState(_mainViewModel.StateHouses);
        }

        public void SelectHouse()
        {

        }

        public void SliceApartment()
        {

        }

        private async void LoadHouses()
        {
            await _mainViewModel.SearchAllHousesAsync(_mainViewModel.SelectedStreet.AppObject);
        }
    }
}