﻿using TestWork.Infrastructure;

namespace TestWork.ViewModels.States
{
    public interface IMainWindowViewModelState
    {
        MainWindowState CurrentMainWindowState { get; }
        bool VisibleBack { get; }
        bool VisibleFilter { get; }
        void Back();
        void SelectCity();
        void SelectStreet();
        void SelectHouse();
        void SliceApartment();
    }
}