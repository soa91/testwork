﻿using TestWork.Infrastructure;
using TestWork.ViewModels.Windows;

namespace TestWork.ViewModels.States
{
    public class MainWindowViewModelStateApartments : IMainWindowViewModelState
    {
        private MainWindowViewModel _mainViewModel;

        public MainWindowViewModelStateApartments(MainWindowViewModel mainWindowViewModel)
        {
            _mainViewModel = mainWindowViewModel;
            CurrentMainWindowState = MainWindowState.Apartments;
            VisibleBack = true;
            VisibleFilter = true;
        }

        public MainWindowState CurrentMainWindowState { get; }
        public bool VisibleBack { get; }
        public bool VisibleFilter { get; }

        public void Back()
        {
            _mainViewModel.SetState(_mainViewModel.StateHouses);
        }

        public void SelectCity()
        {

        }

        public void SelectStreet()
        {

        }

        public void SelectHouse()
        {

        }

        public void SliceApartment()
        {
            if (_mainViewModel.ApartmentAreaMin <= _mainViewModel.ApartmentAreaMax)
            {
                LoadSlice();
            }
        }

        private async void LoadSlice()
        {
            await _mainViewModel.SliceApartmentsAsync(_mainViewModel.SelectedHouse.AppObject,
                _mainViewModel.ApartmentAreaMin,
                _mainViewModel.ApartmentAreaMax);
        }
    }
}
