﻿using TestWork.Infrastructure;
using TestWork.ViewModels.Windows;

namespace TestWork.ViewModels.States
{
    public class MainWindowStateHouses : IMainWindowViewModelState
    {
        private MainWindowViewModel _mainViewModel;

        public MainWindowStateHouses(MainWindowViewModel mainWindowViewModel)
        {
            _mainViewModel = mainWindowViewModel;
            CurrentMainWindowState = MainWindowState.Houses;
            VisibleBack = true;
            VisibleFilter = false;
        }

        public MainWindowState CurrentMainWindowState { get; }
        public bool VisibleBack { get; }
        public bool VisibleFilter { get; }

        public void Back()
        {
            _mainViewModel.SetState(_mainViewModel.StateStreets);
        }

        public void SelectCity()
        {

        }

        public void SelectStreet()
        {

        }

        public void SelectHouse()
        {
            if (_mainViewModel.SelectedHouse == null) return;

            LoadApartments();

            _mainViewModel.SetState(_mainViewModel.StateApartments);
        }

        public void SliceApartment()
        {

        }

        private async void LoadApartments()
        {
            await _mainViewModel.SearchAllApartmentsAsync(_mainViewModel.SelectedHouse.AppObject);
        }
    }
}