﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace TestWork.Views.Converters
{
    public class BooleanToVisibilityLiteConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Visibility result = Visibility.Visible;

            if (value == null)
            {
                result = Visibility.Collapsed;
            }
            else if (value is bool)
            {
                var valStr = (bool) value;

                if (valStr)
                {
                    result = Visibility.Visible;
                }
                else
                {
                    result = Visibility.Collapsed;
                }
            }

            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var result = true;

            if (value == null)
            {
                result = false;
            }
            else if (value is Visibility)
            {
                var visVal = (Visibility) value;

                switch (visVal)
                {
                    case Visibility.Visible:
                        result = true;
                        break;
                    case Visibility.Hidden:
                    case Visibility.Collapsed:
                    default:
                        result = false;
                        break;

                }
            }

            return result;
        }
    }
}
