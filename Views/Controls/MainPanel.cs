﻿using System;
using System.Windows;
using TestWork.Infrastructure;

namespace TestWork.Views.Controls
{
    public class MainPanel : System.Windows.Controls.Control
    {
        // Using a DependencyProperty as the backing store for Cities.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CitiesProperty =
            DependencyProperty.Register("Cities", typeof(object), typeof(MainPanel), null);

        // Using a DependencyProperty as the backing store for Streets.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty StreetsProperty =
            DependencyProperty.Register("Streets", typeof(object), typeof(MainPanel), null);

        // Using a DependencyProperty as the backing store for Houses.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HousesProperty =
            DependencyProperty.Register("Houses", typeof(object), typeof(MainPanel), null);

        // Using a DependencyProperty as the backing store for Apartments.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ApartmentsProperty =
            DependencyProperty.Register("Apartments", typeof(object), typeof(MainPanel), null);

        // Using a DependencyProperty as the backing store for MainWindowState.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MainWindowStateProperty =
            DependencyProperty.Register("MainWindowState", typeof(MainWindowState), typeof(MainPanel),
                new PropertyMetadata(MainWindowState.None, new PropertyChangedCallback(OnStateChanged)));

        public object Cities
        {
            get { return (object)GetValue(CitiesProperty); }
            set { SetValue(CitiesProperty, value); }
        }

        public object Streets
        {
            get { return (object)GetValue(StreetsProperty); }
            set { SetValue(StreetsProperty, value); }
        }

        public object Houses
        {
            get { return (object)GetValue(HousesProperty); }
            set { SetValue(HousesProperty, value); }
        }

        public object Apartments
        {
            get { return (object)GetValue(ApartmentsProperty); }
            set { SetValue(ApartmentsProperty, value); }
        }

        public MainWindowState MainWindowState
        {
            get { return (MainWindowState)GetValue(MainWindowStateProperty); }
            set
            {
                SetValue(MainWindowStateProperty, value);
                ChangeVisualState(true);
            }
        }

        private static void OnStateChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MainPanel c = d as MainPanel;
            if (c != null)
            {
                c.ChangeVisualState(true);
            }
        }

        private void ChangeVisualState(bool useTransitions)
        {
            switch (MainWindowState)
            {
                case MainWindowState.Cities:
                    VisualStateManager.GoToState(this, "Cities", useTransitions);
                    break;
                case MainWindowState.Streets:
                    VisualStateManager.GoToState(this, "Streets", useTransitions);
                    break;
                case MainWindowState.Houses:
                    VisualStateManager.GoToState(this, "Houses", useTransitions);
                    break;
                case MainWindowState.Apartments:
                    VisualStateManager.GoToState(this, "Apartments", useTransitions);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            
            UIElement state1 = Cities as UIElement;
            UIElement state2 = Streets as UIElement;
            UIElement state3 = Houses as UIElement;
            UIElement state4 = Apartments as UIElement;

            if (state1 != null)
                state1.Visibility = MainWindowState == MainWindowState.Cities ? Visibility.Visible : Visibility.Hidden;
            if (state2 != null)
                state2.Visibility = MainWindowState == MainWindowState.Streets ? Visibility.Visible : Visibility.Hidden;
            if (state3 != null)
                state3.Visibility = MainWindowState == MainWindowState.Houses ? Visibility.Visible : Visibility.Hidden;
            if (state4 != null)
                state4.Visibility = MainWindowState == MainWindowState.Apartments ? Visibility.Visible : Visibility.Hidden;
        }
    }
}

